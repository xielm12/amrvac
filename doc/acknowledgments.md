# Acknowledgments

This version of the MPI-AMRVAC software was initiated in the course of 2006-2007
by **Bart van der Holst** (meanwhile at University of Michigan) in a close
collaboration with [Rony Keppens ](http://perswww.kuleuven.be/~u0016541) at the
[Centre for mathematical Plasma-Astrophysics (CmPA)](https://wis.kuleuven.be/CmPA)
at K.U.Leuven.

The code has witnessed continuous improvements and additions, with **Zakaria
Meliani** as the main core developer since 2005-2006 (for the relativistic
physics modules, parallel conversion of MPI-AMRVAC I/O to a large variety of
postprocessing formats for later visualization, and for many overall code
additions/improvements), and **Oliver Porth** joining in on the core team
since 2010.

Other contributors to the project are **Allard Jan van Marle** (for optically
thin radiative loss treatments), **Peter Delmont** (some of the visualization
capabilities), **Chun Xia** ([an]isotropic thermal conduction) and the growing
number of users at CPA and abroad.

The work benefitted from funding by K.U.Leuven GOA/2009/009; FWO project
G.0277.08; and FP7 project [SWIFF](http://swiff.eu).

We thank the following people for their contribution to (earlier versions of)
the software development:

    Gabor Toth                   For the original VAC software
    Hans Goedbloed               Scientific and Financial (project) support
    Dries Kimpe                  valuable expertise on MPI parallelism, general computing support
    Peter Delmont                visualisation efforts and challenging tests
    Hubert Baty                  original and challenging applications
    Margreet Nool                early original AMRVAC coding efforts

\todo Update the acknowledgments
